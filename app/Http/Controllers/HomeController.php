<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    public function index()
    {
        $recipe = new Recipe();
        $recipe->name = "Test";
        $recipe->save();

        return Response("ok");
    }
}
